<?php
/**
 * Created by PhpStorm.
 * User: fatma
 * Date: 10/27/20
 * Time: 1:43 AM
 */

$con = mysqli_connect("localhost", "root", "123456", "php_blog");

// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

//SELECT ALL USERS
$query = "SELECT * FROM `users`";

if(isset($_GET['search'])){
    $search= mysqli_escape_string($con,$_GET['search']);
    $query.= " WHERE `users`.`name` LIKE '%".$search."%' OR `users`.`email` LIKE '%".$search."%'";
}



// Perform query
$result = mysqli_query($con, $query);
?>

<html>

<head>

    <title> Admins :: List Users </title>
</head>
<body>

<h1> LIST USERS </h1>

<form method="GET">
    <input type="text" name="search" placeholder="Enter {NAME} or {Email} to Search"   />
    <input type="submit" value="search">
</form>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>EMAIL</th>
        <th>ADMIN</th>
        <th>ACTION</th>
    </tr>
    </thead>

    <tbody>
    <?php
    while ($row = mysqli_fetch_assoc($result)) {
        ?>
        <tr>
            <td><?= $row['id'] ?></td>
            <td><?= $row['name'] ?></td>
            <td><?= $row['email'] ?></td>
            <td><?= $row['admin'] ? 'Yes' : 'NO'?></td>
            <td>
                <a href="edit.php?id=<?= $row['id'] ?>">Edit</a> ||  <a href="delete.php?id=<?= $row['id'] ?>">Delete</a>
            </td>


        </tr>
    <?php
    }
    ?>
    </tbody>

    <tfoot>
        <tr>
            <td colspan="2" style="text-align: center"> <?=  mysqli_num_rows($result)?></td>
            <td colspan="2" style="text-align: center"><a href="add.php">Add User </a></td>
        </tr>

    </tfoot>

</table>


</body>

</html>


<?=
//CLOSE THE CONNECTION
mysqli_free_result($result);
mysqli_close($con);
?>
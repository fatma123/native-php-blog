<?php
/**
 * Created by PhpStorm.
 * User: fatma
 * Date: 10/23/20
 * Time: 12:55 AM
 */

$con = mysqli_connect("localhost","root","123456","php_blog");

// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

$query= "SELECT * FROM `users`";
// Perform query
$result = mysqli_query($con, $query);

while($row = mysqli_fetch_assoc($result)){

    echo "ID: ".    $row['id']."<br />";
    echo "NAME: ".  $row['name']."<br />";
    echo "EMAIL: ". $row['email']."<br />";
    echo str_repeat("-", 50)."<br />";
}



//CLOSE THE CONNECTION
mysqli_free_result($result);
mysqli_close($con);


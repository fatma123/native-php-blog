<?php
/**
 * Created by PhpStorm.
 * User: fatma
 * Date: 10/23/20
 * Time: 1:22 AM
 */

//VALIDATION

$error_fields = array();



if( ! (isset($_POST['name']) && !empty($_POST['name']) )){

    $error_fields[] = "name";

}

if( ! (isset($_POST['email']) && filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) )){

    $error_fields[] = "email";

}
if( ! (isset($_POST['password']) && strlen($_POST['password'] )>5)){

    $error_fields[] = "password";

}

if($error_fields){

//    var_dump($error_fields);
    header("Location: form.php?error_fields=".implode("," ,$error_fields));
    exit;
}

$con = mysqli_connect("localhost","root","123456","php_blog");
// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

//ESCAPE ANY SPECIAL CHARACTERS TO AVOID SQL INJECTION


